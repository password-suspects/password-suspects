from itertools import product, permutations
import sys
import math

# Function to find permutation formula i.e n!/(n-r)!
def npr(n, r):
    i = int(n / r)
    r = 1
    return math.factorial( i )/(math.factorial( i - r ))

#Function to get all possible repetitions
def all_repetitions(string1, remain_no) :
    outcomes = []
    for c in product(string1, repeat = remain_no) :
       outcomes.append(c)
    return outcomes


def main_fun(leng, no_str) :

    if leng == 0 and no_str == 0 :
       quit()
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    string_list = []

    if 1 <= leng <= 25 and 0 <= no_str <= 10 :
       for i in range(3, no_str+3) :
          string_list.append(sys.argv[ i ])#passing strings which are taken through command prompt into a list

    len_str = 0   #finding length of the strings passed
    for i in string_list :
       if 1 <= len(i) <= 25 :
          len_str += len(i)


# case 1: If entered strings lengths is 0 i.e no strings entered

    sample_list1 = []
    if no_str == 0 :
       if leng == 1:
          for i in all_repetitions(alphabet, leng):
             sample_list1.append(''.join( i ))
          return 26 ** leng, sample_list1

       elif  1 < leng < 25:
          return 26 ** leng, string_list

#case 2: If entered strings length is equal is actual password length

    sample_list2 = []
    if len_str == leng :
       for i in sorted(set(permutations(string_list))) :
          sample_list2.append(''.join(i))
       return len(sample_list2), sample_list2

#case 3: If entered strings length is less than actual password length

    elif len_str < leng :
       length1 = leng
       total = 1
       for i in range(0, len(string_list)):
          g = len(string_list[i])
          total *= npr(length1, g)
          length1 = length1 - g
          n_ways = total *(26 ** (leng - len_str))
          sample_list3 = []
          sample_list4 = []

       for j in all_repetitions(alphabet, (leng - len_str)) :
          sample_list3.append(''.join(j))
       for i in range(0, len(sample_list3)) :
          string_list.append(sample_list3[i])
          for j in sorted(set(permutations(string_list))) :
             sample_list4.append(''.join(j))
          string_list.remove(sample_list3[i])
       return n_ways, sample_list4

leng = int(sys.argv[1])
no_str =int(sys.argv[2])

final_str = []
possible_values, final_str = main_fun(leng, no_str)
print(possible_values, "suspects")

def print_possible_values(possible_values,final_str):    
    if possible_values < 42:
       return final_str
       
    else:
       return quit()

for  i in print_possible_values(possible_values, final_str):
    print(i)








